const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');

const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    telefono: /^\d{9,10}$/, // 9 a 14 numeros.
    cedula: /^\d{10}$/, // 10 numeros.
    direccion: /^[a-zA-Z0-9\s]{7,20}$/ // Letras, numeros, guion y guion_bajo
}

const valid = {
    cedula: false,
    apellidos: false,
    nombre:false,
    ciudad:false,
    direccion:false,
    telefono:false,
    correo:false
}

const validarFormulario = (e) => {
    switch (e.target.name){
        case "cedula":
            validarCampoDinamico(expresiones.cedula, e.target, 'cedula');
        break;
        
        case "apellidos":
            validarCampoDinamico(expresiones.nombre, e.target, 'apellidos');
        break;
        
        case "nombres":
            validarCampoDinamico(expresiones.nombre, e.target, 'nombres');
        break;

        //case "estado_civil":
            
        //break;

        //case "genero":
            
        //break;

        case "ciudad":
            validarCampoDinamico(expresiones.nombre, e.target, 'ciudad');
        break;

        case "direccion":

            validarCampoDinamico(expresiones.direccion, e.target, 'direccion');
        break;

        case "telefono":
            validarCampoDinamico(expresiones.telefono, e.target, 'telefono');
        break;

        case "correo":
            validarCampoDinamico(expresiones.correo, e.target, 'correo');
        break;
    }
}
const validarCampoDinamico = (expresion, input, campo) => {

    if(expresion.test(input.value)){
        document.getElementById(`grupo_${campo}`).classList.remove('formulario_grupo-incorrecto');
        document.getElementById(`grupo_${campo}`).classList.add('formulario_grupo-correcto');
        document.querySelector(`#grupo_${campo} .formulario_input-error`).classList.remove('formulario_input-error-activo');
        valid[campo] = true;
    }else{
        document.getElementById(`grupo_${campo}`).classList.add('formulario_grupo-incorrecto');
        document.querySelector(`#grupo_${campo} .formulario_input-error`).classList.add('formulario_input-error-activo');
        valid[campo] = false;
    }

}

inputs.forEach((input) => {
    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
    e.preventDefault();

    if (valid.cedula && valid.apellidos && valid.nombres && valid.ciudad &&  valid.direccion && valid.telefono && valid.correo){

        document.cookie = nombres + apellidos;
        alert("Nombre = " + nombres.value + " Apellidos" + apellidos.value
        + " Cedula = " + cedula.value + " Ciudad =" + ciudad.value + " Direccion = " 
        + direccion.value + " Telefono =" + telefono.value + " Correo Electronico =" +correo.value);

        formulario.reset();
        document.getElementById('formulario_mensaje-exito').classList.add('formulario_mensaje-exito-activo');
        setTimeout(()=>{
            document.getElementById('formulario_mensaje-exito').classList.remove('formulario_mensaje-exito-activo');

        }, 5000);
    }else{
        alert("Hacen falta datos en el formulario");
    }
});
