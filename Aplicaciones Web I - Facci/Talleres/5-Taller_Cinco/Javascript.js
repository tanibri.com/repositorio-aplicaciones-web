var meses = ["Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"];
            
document.write(`<h4>Meses del Año</h4>`);
document.write(`<br>`);

for (var i = 0 ; i < meses.length ; i++){
    document.write(" "+meses[i]);
}
            
document.write(`<br>`);
class Producto_alimenticio{
    constructor(codigo, nombre, precio){
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
    }
}

var AlimentoUno = new Producto_alimenticio("10","Cola","10,50");
var AlimentoDos = new Producto_alimenticio("20","Sal","20,00");
var AlimentoTres = new Producto_alimenticio("30","Pimienta","50,75");
imprimeDatos();
            
function imprimeDatos(){
    
    var array = new Array();
    array[0]=AlimentoUno;
    array[1]=AlimentoDos;
    array[2]=AlimentoTres;
    document.write(`<h4>Productos Alimenticios</h4>`);
    document.write(`<br>`)

    for (var i = 0 ; i < 3 ; i++){
        var valores = array[i];
        document.write(`${valores.codigo}` + " ");
    }
    document.write(`<br>`)

    for (var i = 0 ; i < 3 ; i++){
        var valores = array[i];
        document.write(`${valores.nombre + " "}`);
    }
    document.write(`<br>`)
    
    for (var i = 0 ; i < 3 ; i++){
        var valores = array[i];
        document.write(`${valores.precio}` + " ");
    }
    
}