function f_horas_segundos(){
    var hora = new Date().getHours();
    var minutos = new Date().getMinutes();
    var segundos = new Date().getSeconds();
    document.getElementById("id_hora_actual").innerHTML=hora + ":"+minutos+":"+segundos +"<br>";
    var conversion = hora * 3600;
    document.getElementById("id_mostrar_segundos").innerHTML = "Hora Actual a Segundos = <br> "+conversion;
}

function f_area_triangulo(){
    var base = parseFloat (document.getElementById("id_base").value);
    var altura = parseFloat(document.getElementById("id_altura").value);
    var resultado_area_triangulo = (base * altura) / 2;
    console.log(resultado_area_triangulo);
    document.getElementById("id_resultado_area_triangulo").value=resultado_area_triangulo;
}

function f_raiz_cuadrada(){
    var raiz = parseFloat(document.getElementById("id_raiz").value);
    var resultado_raiz = Math.sqrt(raiz).toPrecision(3);
    console.log(resultado_raiz);
    document.getElementById("id_resultado_raiz_cuadrada").value=resultado_raiz;
}

function f_caracteres(){
    var total_caracteres = String(document.getElementById("id_caracter").value);
    var resultado_caracter = total_caracteres.length;
    document.getElementById("id_resultado_caracter").value=resultado_caracter;
}

function f_concatenar(){
    var array1 = ["Lunes", " Martes", " Miercoles", " Jueves", " Viernes"];
    var array2 = [", Sabado"," Domingo"];
    var concatenar = array1 + array2;
    document.getElementById("id_resultado_concatenar").innerHTML = concatenar;
}

function f_version_de_navegador(){
    var version = navigator.appVersion;
    document.getElementById("id_version_app").innerHTML = version;
}

function f_tamaño_pantalla(){
    var y = screen.width;
    var x = screen.height;
    document.getElementById("id_resolucion").innerHTML = "Largo = " + y +" x Alto = " + x;
}

function f_imprimir_pagina(){
    window.print();
}
